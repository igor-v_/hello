package com.example.hello.db

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
//import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
public interface OperationRepository extends PagingAndSortingRepository<Operation, String> {
  public Page<Operation> findAll(Pageable pageable)
}
