package com.example.hello.db

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
public interface VisitRepository extends CrudRepository<Visit, Long> {
  public List<Visit> findTop20ByOrderByTimeDesc()
}
