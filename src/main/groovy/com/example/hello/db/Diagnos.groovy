package com.example.hello.db

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name='t_mkb')
public class Diagnos {
  @Id
  @Column(name = 'code', length = 7)
  public String icd10
  
  @Column(length = 300)
  public String name
}
