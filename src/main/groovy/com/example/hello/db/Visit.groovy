package com.example.hello.db

import javax.persistence.Column
import javax.persistence.Entity
//import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name='t_visit')
public class Visit {
  @Id
//  @GeneratedValue
  public Long id = 0
  
  @Column(length = 30)
  public String ip
  
  @Column(length = 200)
  public String agent
  
  @Column(length = 50)
  public String time
  
//  public Visit() {
//    this.id = 0
//  }
}