package com.example.hello.db

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name='v_oper')
public class Operation {
  @Id
  @Column(name = 'code', length = 10)
  public String code
  
  @Column(length = 50)
  public String briefName
}
