package com.example.hello.controllers

import java.util.HashMap
import java.util.Map
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
//import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.ModelAndView

import com.example.hello.db.DiagnosRepository

@Controller
@RequestMapping('/db')
public class DbController {

  final DiagnosRepository diagnosRepository

  public DbController(DiagnosRepository diagnosRepository) {
    this.diagnosRepository = diagnosRepository
  }  
  
  @GetMapping("/{page}")
	public ModelAndView db(@PathVariable('page') String page) {
    
  	Map<String, Object> model = new HashMap<>()
    int cnt_recs = diagnosRepository.count(), cnt_pages
    
    model.put('cnt', cnt_recs)
    
    cnt_pages = cnt_recs / 20
    if (cnt_pages % 20) cnt_pages++
    def pages = new int[cnt_pages]
    cnt_pages.times { pages[it] = it+1 } 

    model.put('pages', pages)
    model.put('list', diagnosRepository.findAll(PageRequest.of(page.toInteger()-1, 20, Sort.by('icd10'))))
    
  	return new ModelAndView('db', model)

	}
	
}
