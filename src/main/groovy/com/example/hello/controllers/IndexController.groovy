package com.example.hello.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.servlet.ModelAndView

@Controller
public class IndexController {

  @GetMapping('/')
	public ModelAndView index(@RequestHeader Map<String, String> headers) {
  	return new ModelAndView("index")
	}
	
}
