package com.example.hello.controllers

import java.sql.Timestamp
import java.util.ArrayList
import java.util.Arrays
import java.util.HashMap
import java.util.List
import java.util.Map

import javax.servlet.http.HttpServletRequest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.servlet.ModelAndView

import com.example.hello.db.Visit
import com.example.hello.db.VisitRepository
import com.example.hello.Util

@Controller
public class IpController {

  @Autowired 
  private HttpServletRequest request
  final VisitRepository visitRepository

  public IpController(VisitRepository visitsRepository) {
    this.visitRepository = visitsRepository
  }  
  
  @GetMapping('/ip')
	public ModelAndView ip(@RequestHeader Map<String, String> headers) {
	
  	Map<String, Object> model = new HashMap<>()
  	Visit visit = new Visit()
  	
  	model.put('time', visit.time = new Timestamp(System.currentTimeMillis()).toString())
  	model.put('ip', visit.ip = Util.getClientIpAddress(request))
  	model.put('agent', visit.agent = request.getHeader('user-agent'))
    model.put('last', visitRepository.findTop20ByOrderByTimeDesc())
  
    visitRepository.save(visit)
    
  	return new ModelAndView('ip', model)

	}
	
}
