package com.example.hello.controllers

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

import com.example.hello.db.Visit
import com.example.hello.db.VisitRepository

@RestController
@RequestMapping('/api')
public class ApiController {

  final VisitRepository visitRepository

  public ApiController(VisitRepository visitsRepository) {
    this.visitRepository = visitsRepository
  }

  @GetMapping('/visits')
  public Iterable<Visit> getVisits() {
    return visitRepository.findAll()
  }

}
