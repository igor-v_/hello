package com.example.hello

import javax.servlet.http.HttpServletRequest

public class Util { 

  public static String getClientIpAddress(HttpServletRequest request) {
    
    def res = request.getRemoteAddr()
    [ 
      'X-Forwarded-For',
      'Proxy-Client-IP',
      'WL-Proxy-Client-IP',
      'HTTP_X_FORWARDED_FOR',
      'HTTP_X_FORWARDED',
      'HTTP_X_CLUSTER_CLIENT_IP',
      'HTTP_CLIENT_IP',
      'HTTP_FORWARDED_FOR',
      'HTTP_FORWARDED',
//      'Accept-Encoding', 
      'HTTP_VIA',
      'REMOTE_ADDR'
    ].any {  
      def ip = request.getHeader(it)
      if (ip) res = ip
//      println "${it}: ${ip}"
      ip
    }
    res
  }

}

//package com.example.hello
//
//import javax.servlet.http.HttpServletRequest
//
//public class Util {
//
//  public static String getClientIpAddress(HttpServletRequest request) {
//
//    String[] IP_HEADER_CANDIDATES = {
//      "X-Forwarded-For",
//      "Proxy-Client-IP",
//      "WL-Proxy-Client-IP",
//      "HTTP_X_FORWARDED_FOR",
//      "HTTP_X_FORWARDED",
//      "HTTP_X_CLUSTER_CLIENT_IP",
//      "HTTP_CLIENT_IP",
//      "HTTP_FORWARDED_FOR",
//      "HTTP_FORWARDED",
//      "HTTP_VIA",
//      "REMOTE_ADDR"
//    }
//
//    for (String header : IP_HEADER_CANDIDATES) {
//      String ip = request.getHeader(header)
//      if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) return ip
//    }
//    return request.getRemoteAddr()
//  }
//
//}
